package managers;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import mhgaber.com.screeninteraction.R;
import customviews.RoundImage;
import models.ScreeninteractionAppcontroller;

/**
 * Created by Gaber on 10/8/15.
 */
public class ImageLoader  {
    public static void loadImage(final ImageView imageView, final String url, final int position, final boolean rounded,
                                 final boolean checkPos, final boolean animate){
        imageView.setImageResource(R.drawable.transparent);
        new AsyncTask<Void,Void,Void>(){
            Bitmap bitmap;
            RoundImage roundImage;
            boolean cacheHit;
            @Override
            protected Void doInBackground(Void... voids) {
                CachManager cachManager=new CachManager(ScreeninteractionAppcontroller.screeninteractionAppcontroller);

                 bitmap=cachManager.loadImageFromDisk(url);
                if(bitmap==null){
                    cacheHit=false;
                bitmap=getBitmapFromURL(url);
                }else{
                    cacheHit=true;
                }

                if(rounded&&bitmap!=null){
                    roundImage=new RoundImage(bitmap);
                }
                if(!cacheHit)cachManager.saveImageToDisk(bitmap,url);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                AlphaAnimation animation=new AlphaAnimation(0f,1f);
                animation.setDuration(1000);
                if(animate)
                    imageView.startAnimation(animation);
                if(checkPos){
                if((int)imageView.getTag()==position){
                    if(rounded)
                        imageView.setImageDrawable(roundImage);
                    else
                    imageView.setImageBitmap(bitmap);

                }
                super.onPostExecute(aVoid);
            }
            else{

                    if(rounded)
                        imageView.setImageDrawable(roundImage);
                    else
                        imageView.setImageBitmap(bitmap);


                super.onPostExecute(aVoid);
            }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
    public static void loadImage(final ImageView imageView, final String url, final int position, final boolean rounded,
                                 final boolean checkPos){
        loadImage(imageView, url, position, rounded,
                checkPos, false);
    }
    public static void loadImage(ImageView iv,String url,int pos){
        loadImage(iv,url,pos,false,true);
    }
    public static void loadImage(ImageView iv,String url){
        loadImage(iv,url,-1,false,false);
    }
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    public static void loadImageWithAnimation(ImageView iv, String picureUrl) {
        loadImage(iv,picureUrl,-1,false,false,true);
    }
}
