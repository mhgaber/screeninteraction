package models;
import java.io.Serializable;

/**
 * Created by Gaber on 10/8/15.
 */
public class Contact implements Serializable {

    public String name;
    public String jobTitle;
    public String email;
    public String phone;
    public String webPageUrl;
    public String picureUrl;
    public String thumbnailUrl;
    public boolean favroite;
    public boolean isSectionHeader;
    public char secionChar;
}
