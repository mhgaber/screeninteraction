package parsers;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import models.ScreeninteractionAppcontroller;
import models.Contact;

/**
 * Created by Gaber on 10/8/15.
 */
public class Jparser  {

    public  static ArrayList <Contact> parseContacts(String string){
        ArrayList<Contact>contactsList=new ArrayList<>();
        try {
            JSONArray jsonArray=new JSONArray(string);
            for (int i = 0; i < jsonArray.length(); i++) {
                contactsList.add(parseContact(jsonArray.getJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  contactsList;
    }
    public static Contact parseContact(JSONObject jsonObject){
        Contact contact =new Contact();
        try {

            contact.name=jsonObject.getString("name");
            contact.jobTitle=jsonObject.getString("job_title");
            contact.phone=jsonObject.getString("phone");
            contact.email=jsonObject.getString("email");
            contact.webPageUrl=jsonObject.getString("webpage");
            contact.picureUrl=jsonObject.getString("picture-url");
            contact.thumbnailUrl=jsonObject.getString("thumbnail-url");
            contact.favroite= ScreeninteractionAppcontroller.favoritePrefs.getBoolean(contact.name,false);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return contact;
    }
}
