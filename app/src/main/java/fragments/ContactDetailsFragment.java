package fragments;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import managers.ImageLoader;
import mhgaber.com.screeninteraction.R;
import mhgaber.com.screeninteraction.WebBrowserActivity;
import models.Contact;


public class ContactDetailsFragment extends Fragment implements View.OnClickListener {
    Contact contact;
    View parenView;
    public ImageView profileThumbIv;
    ImageView fullPicIv;
    public TextView nameTv;
    TextView emailTv;
    TextView websiteTv;
    TextView phoneTv;
    ImageView websiteIv, hangoutIv, emailIv, phoneIv, backIv, editIV;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parenView = inflater.inflate(R.layout.fragment_contact_details, null);
        initComponents();
        return parenView;
    }

    private void initComponents() {
        profileThumbIv = (ImageView) findViewById(R.id.imageViewProfileThump);
        nameTv = (TextView) findViewById(R.id.textViewName);
        fullPicIv = (ImageView) findViewById(R.id.imageViewFullProfilePic);
        emailTv = (TextView) findViewById(R.id.textViewEmail);
        websiteTv = (TextView) findViewById(R.id.textViewWebSite);
        phoneTv = (TextView) findViewById(R.id.textViewPhoneNumber);
        websiteIv = (ImageView) findViewById(R.id.imageViewWebsite);
        hangoutIv = (ImageView) findViewById(R.id.imageViewHangouts);
        emailIv = (ImageView) findViewById(R.id.imageViewEmail);
        phoneIv = (ImageView) findViewById(R.id.imageViewPhone);
        backIv = (ImageView) findViewById(R.id.imageViewBack);
        editIV = (ImageView) findViewById(R.id.imageViewEdit);
        editIV.setOnClickListener(this);
        backIv.setOnClickListener(this);
        findViewById(R.id.phoneSectoion).setOnClickListener(this);
        findViewById(R.id.mailSection).setOnClickListener(this);
        findViewById(R.id.webSiteSecion).setOnClickListener(this);

    }

    public void setContact(Contact contact) {
        this.contact = contact;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            window.setStatusBarColor(getActivity().getResources().getColor(R.color.colorSplashBackGround));

        }
        ;
        updateUi();
    }

    private void updateUi() {
        nameTv.setText(contact.name);
        emailTv.setText(contact.email);
        phoneTv.setText(contact.phone);
        websiteTv.setText(contact.webPageUrl);
        ImageLoader.loadImageWithAnimation(fullPicIv, contact.picureUrl);
    }

    private View findViewById(int res) {
        return parenView.findViewById(res);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Intent intent;
        switch (id) {

            case R.id.imageViewBack:
                getActivity().onBackPressed();
                break;
            case R.id.phoneSectoion:
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + contact.phone));
                startActivity(callIntent);
                break;
            case R.id.mailSection:
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO,
                        Uri.parse("mailto:" + Uri.encode(contact.email)));
                if (emailIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(emailIntent);
                } else {
                    Toast.makeText(getActivity(), "cant find an email client", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.webSiteSecion:
                intent = new Intent(getActivity(), WebBrowserActivity.class);
                intent.putExtra("url", contact.webPageUrl);
                startActivity(intent);
                break;
            case R.id.imageViewEdit:
                intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                intent.putExtra(ContactsContract.Intents.Insert.EMAIL, contact.email);
                intent.putExtra(ContactsContract.Intents.Insert.PHONE, contact.phone);
                intent.putExtra(ContactsContract.Intents.Insert.JOB_TITLE, contact.jobTitle);
                startActivity(intent);

                break;
        }
    }
}
