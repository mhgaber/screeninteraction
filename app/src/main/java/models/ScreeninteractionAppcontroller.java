package models;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.util.ArrayList;
import java.util.Comparator;

import managers.InternetRequestManger;

/**
 * Created by Gaber on 10/8/15.
 */
public class ScreeninteractionAppcontroller extends Application  {
    public  static ScreeninteractionAppcontroller screeninteractionAppcontroller;
    public static InternetRequestManger internetRequestManger;
    public static ArrayList<Contact>contactsList;
    public static Comparator <Contact> aToZ,ztoA;
    public static SharedPreferences favoritePrefs;
    @Override
    public void onCreate() {
        super.onCreate();
        screeninteractionAppcontroller =this;
        internetRequestManger=new InternetRequestManger();
        initiateComparators();
        favoritePrefs=getSharedPreferences("favPrefs",MODE_PRIVATE);

    }

    private void initiateComparators() {
        aToZ=new Comparator<Contact>() {
            @Override
            public int compare(Contact contact1, Contact contact2) {
                return contact1.name.compareToIgnoreCase(contact2.name);
            }
        };
        ztoA=new Comparator<Contact>() {
            @Override
            public int compare(Contact contact1, Contact contact2) {
                return contact2.name.compareToIgnoreCase(contact1.name);
            }
        };
    }

    public static boolean isConnectedToInternet() {


        ConnectivityManager connectivity = (ConnectivityManager) ScreeninteractionAppcontroller.screeninteractionAppcontroller.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (NetworkInfo anInfo : info)
                    if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }
    public static void debug(String message) {
        Log.d("MYTAG", message);
    }
    public static void toggleFavorite(String name,boolean makeFavorited){
        if(makeFavorited)favoritePrefs.edit().putBoolean(name, true).apply();
        else favoritePrefs.edit().remove(name).apply();
    }
}
