package managers;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created by Gaber on 11/10/15.
 */
public class CachManager {
    Context context;
    SharedPreferences responsePrefrences;
    private static final String RESPONSEPREFS="responsePrefs";
    public CachManager(Context context){
        this.context=context;
        responsePrefrences=context.getSharedPreferences(RESPONSEPREFS,Context.MODE_PRIVATE);
    }
    public void saveImageToDisk(Bitmap bitmap,String key){
        if(key.length()==0)return;
        key=key.hashCode()+"";
            if(bitmap==null)return;
            // path to /data/data/yourapp/app_data/imageDir
            File directory = context.getDir("imageDir", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath=new File(directory,"image"+key+".jpg");

            FileOutputStream fos = null;
            try {

                fos = new FileOutputStream(mypath);

                // Use the compress method on the BitMap object to write image to the OutputStream
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.close();

            } catch (Exception e) {
                e.printStackTrace();
            }


    }
    public Bitmap loadImageFromDisk(String key){
        if (key.length()==0)return null;
        key=key.hashCode()+"";

        File directory = context.getDir("imageDir", Context.MODE_PRIVATE);

        try {
            File f=new File(directory,"image"+key+".jpg");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f.getAbsoluteFile()));
            return b;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    public String getCachedResponse (String key){
        String ret=responsePrefrences.getString(key,null);

        return  ret;
    }
    public void saveResponse(String url,String response){
        responsePrefrences.edit().putString(url,response).commit();

    }
}
