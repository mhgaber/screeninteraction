package managers;

/**
 * Created by Gaber on 10/8/15.
 */

import android.os.AsyncTask;
import android.support.v4.util.Pair;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import callbacks.WebRequestCallBack;
import models.ScreeninteractionAppcontroller;

public class InternetRequestManger {

    String error = null;

    public InternetRequestManger() {

    }

    public static String unescapeJava(String escaped) {
        if (escaped.indexOf("\\u") == -1)
            return escaped;

        String processed = "";

        int position = escaped.indexOf("\\u");
        while (position != -1) {
            if (position != 0)
                processed += escaped.substring(0, position);
            String token = escaped.substring(position + 2, position + 6);
            escaped = escaped.substring(position + 6);
            processed += (char) Integer.parseInt(token, 16);
            position = escaped.indexOf("\\u");
        }
        processed += escaped;

        return processed;
    }

    public void startGetRequest(String url, HashMap<String, String> params, WebRequestCallBack callBack) {
        startRequest(url, params, callBack, "GET");
    }

    public void startGetRequest(String url, ArrayList<Pair<String, String>> params, WebRequestCallBack callBack) {
        if (params == null) {
            startRequest(url, null, callBack, "GET");
            return;
        }
        if (params.size() > 0) url += "?";
        for (int i = 0; i < params.size(); i++) {
            try {
                url += (params.get(i).first + "=" + URLEncoder.encode(params.get(i).second, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            if (i != params.size() - 1) url += "&";
        }

        startRequest(url, null, callBack, "GET");

    }

    public void startPostRequest(String url, HashMap<String, String> params, WebRequestCallBack callBack) {
        startRequest(url, params, callBack, "POST");
    }

    private void startRequest(final String url, final HashMap<String, String> params, final WebRequestCallBack callBack, final String type) {
        AsyncTask<Void, Void, String> asyncTask = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... args) {
                String response = "";
                if (type.equals("GET")) {
                    try {
                        response = sendGet(url, params);
                    } catch (Exception e) {
                        error = e.getMessage();
                        e.printStackTrace();
                    }
                } else if (type.equals("POST")) {
                    try {
                        response = sendPost(url, params);

                    } catch (Exception e) {
                        error = e.getMessage();
                        Log.e("MY ERROR", ": request failed with error " + e.getMessage().toString());
                        e.printStackTrace();
                    }
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                if (error == null)
                    callBack.onSuccess(response);
                else {
                    callBack.onError(error);
                    error = null;
                }
            }
        };
        asyncTask.execute();
    }

    private String sendGet(String url, HashMap<String, String> params) throws Exception {
        String urlParameters = "";
        if (params != null) {
            for (String key : params.keySet()) {
                urlParameters += (key + "=" + params.get(key) + "&");
            }
            //remove the last &
            if (urlParameters.endsWith("&"))
                urlParameters = urlParameters.substring(0, urlParameters.length() - 1);
            url += "?" + urlParameters;
        }
        ScreeninteractionAppcontroller.debug(String.format("Sending A get Request with url : %s and params : %s", url, urlParameters));
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is GET
        con.setRequestMethod("GET");

        //add request header

        int responseCode = con.getResponseCode();
        ScreeninteractionAppcontroller.debug("response code : " + responseCode);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        return unescapeJava(response.toString());

    }

    // HTTP POST request
    private String sendPost(String url, HashMap<String, String> params) throws Exception {
        String urlParameters = "";
        if (params != null) {
            for (String key : params.keySet()) {
                key = URLEncoder.encode(key, "UTF-8");
                String value = URLEncoder.encode(params.get(key), "UTF-8");
                urlParameters += (key + "=" + value + "&");
            }
            //remove the last &
            if (urlParameters.endsWith("&"))
                urlParameters = urlParameters.substring(0, urlParameters.length() - 1);
        }
        ScreeninteractionAppcontroller.debug(String.format("Sending A Post Request with url : %s and params : %s", url, urlParameters));
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setReadTimeout(10000);
        con.setConnectTimeout(15000);
        con.setDoInput(true);
        con.setDoOutput(true);


        // Send post request
        OutputStream os = con.getOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(os, "UTF-8"));


        writer.write(urlParameters);
        writer.flush();
        writer.close();
        os.close();

        //con.connect();

        int responseCode = con.getResponseCode();
        ScreeninteractionAppcontroller.debug("post request response code is : " + responseCode);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream(), "UTF-8"));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return unescapeJava(response.toString());


    }


}