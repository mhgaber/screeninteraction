package mhgaber.com.screeninteraction;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.ArrayList;

import callbacks.WebRequestCallBack;
import managers.CachManager;
import models.ScreeninteractionAppcontroller;
import parsers.Jparser;
import utils.Const;

public class SplashActivity extends AppCompatActivity {
    int doneCounter=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_splash);
        getContacts();
        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyDone();
            }
        }, 1000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorSplashBackGround));
        };

    }

    private void getContacts() {
        final CachManager cachManager=new CachManager(ScreeninteractionAppcontroller.screeninteractionAppcontroller);
        final String url= Const.URL;
        if(ScreeninteractionAppcontroller.isConnectedToInternet()){
        ScreeninteractionAppcontroller.internetRequestManger.startGetRequest(url, new ArrayList<Pair<String, String>>(), new WebRequestCallBack() {
            @Override
            public void onSuccess(String response) {
                super.onSuccess(response);
                ScreeninteractionAppcontroller.contactsList = Jparser.parseContacts(response);
                cachManager.saveResponse(url,response);
                notifyDone();
            }

            @Override
            public void onError(String error) {
                String response =cachManager.getCachedResponse(url);
                if(response==null)
                    Toast.makeText(SplashActivity.this, R.string.InternetConnectionProblem, Toast.LENGTH_SHORT).show();
                else {
                    ScreeninteractionAppcontroller.contactsList=Jparser.parseContacts(response);
                    notifyDone();
                }

            }
        });}
        else{

            String response =cachManager.getCachedResponse(url);
            if(response==null)
                Toast.makeText(SplashActivity.this, R.string.InternetConnectionProblem, Toast.LENGTH_SHORT).show();
            else {
                ScreeninteractionAppcontroller.contactsList=Jparser.parseContacts(response);
                notifyDone();
            }
        }
    }
    private synchronized void notifyDone(){
        doneCounter++;
        if(doneCounter==2){
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            finish();
        }
    }

}
