package mhgaber.com.screeninteraction;

import android.os.Build;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import adapters.ContactListAdapter;
import fragments.ContactDetailsFragment;
import models.ScreeninteractionAppcontroller;
import models.Contact;

public class MainActivity extends AppCompatActivity {
    boolean sortAtoZ=true;
    ListView listView;
    RelativeLayout animationHelper,detailsFragmentContainer;
    ImageView animationImageView;
    TextView animationTextView,sortTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorSplashBackGround));
        };

        animationHelper= (RelativeLayout) findViewById(R.id.animationHelper);
        animationImageView= (ImageView) animationHelper.findViewById(R.id.imageViewanimationHelper);
        animationTextView=(TextView)animationHelper.findViewById(R.id.textViewanimationHelper);
        animationImageView.setVisibility(View.INVISIBLE);
        detailsFragmentContainer=(RelativeLayout)findViewById(R.id.detailsFragmentContainer);
        final ArrayList <Contact> list=new ArrayList<>();
        prepareList(list);
        listView = (ListView) findViewById(R.id.listView);
        sortTv =(TextView)findViewById(R.id.textViewSort);
        sortTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sortTv.getText().equals("A->Z")){
                    sortAtoZ=false;
                    prepareList(list);
                    sortTv.setText("Z->A");
                }else{
                    sortAtoZ=true;
                    prepareList(list);
                    sortTv.setText("A->Z");
                }

                AlphaAnimation animation=new AlphaAnimation(1f,0f);
                animation.setDuration(300);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        listView.setAdapter(new ContactListAdapter(MainActivity.this,0,list));
                        listView.startAnimation(new AlphaAnimation(0f,1f));
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                listView.startAnimation(animation);
            }
        });
        final ContactListAdapter adapter = new ContactListAdapter(this,0,list);
        listView.setAdapter(adapter);
        final ContactDetailsFragment contactDetailsFragment=new ContactDetailsFragment();
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.detailsFragmentContainer,contactDetailsFragment).commit();
        detailsFragmentContainer.setVisibility(View.INVISIBLE);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapter.getItem(i).isSectionHeader)return;
                contactDetailsFragment.setContact(adapter.getItem(i));
                int[] locationIV = new int[2];
                int []locationIv2=new int[2];
                int[] locationTv=new int[2];
                int []locationTv2=new int[2];
                //location 3 is calculated only once because all views coordiante at the same pos "top left"
                int []location3=new int[2];
                ViewGroup viewGroup = (ViewGroup)listView.getChildAt(i-listView.getFirstVisiblePosition());
                ImageView thumpIv = (ImageView) viewGroup.findViewById(R.id.imageViewProfileThump);
                TextView listNameTv=(TextView)viewGroup.findViewById(R.id.textViewName);
                TextView detailsNameTv=contactDetailsFragment.nameTv;
                detailsFragmentContainer.setVisibility(View.VISIBLE);
                detailsFragmentContainer.setAlpha(0f);
                animateTextViews(listNameTv,detailsNameTv,animationTextView);
                animateImageViews(thumpIv, contactDetailsFragment.profileThumbIv, animationImageView);
                detailsFragmentContainer.animate().alpha(1f);

            }
        });


    }
    public void animateImageViews(final ImageView fromIv , final ImageView toIv, final ImageView helpeIV){
        int[] location = new int[2];
        int []location2=new int[2];
        int []location3=new int[2];
        fromIv.getLocationOnScreen(location);
        toIv.getLocationOnScreen(location2);
        helpeIV.getLocationOnScreen(location3);
        int fromXDelta=location[0]-location3[0];
        int fromYDelta=location[1]-location3[1];
        int toXDelta=location2[0]-location3[0];
        int toYDelta=location2[1]-location3[1];
        TranslateAnimation animation =new TranslateAnimation(fromXDelta,toXDelta,fromYDelta,toYDelta);
        animation.setDuration(400);
        toIv.setImageDrawable(fromIv.getDrawable());
        helpeIV.setImageDrawable(fromIv.getDrawable());
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                fromIv.setVisibility(View.INVISIBLE);
                toIv.setVisibility(View.INVISIBLE);
                helpeIV.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                fromIv.setVisibility(View.VISIBLE);
                toIv.setVisibility(View.VISIBLE);
                helpeIV.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        helpeIV.startAnimation(animation);

    }
    public void animateTextViews(final TextView fromTv , final TextView toTv, final TextView helperTv){
        int[] location = new int[2];
        int []location2=new int[2];
        int []location3=new int[2];
        fromTv.getLocationOnScreen(location);
        toTv.getLocationOnScreen(location2);
        helperTv.getLocationOnScreen(location3);
        int fromXDelta=location[0]-location3[0];
        int fromYDelta=location[1]-location3[1];
        int toXDelta=location2[0]-location3[0];
        int toYDelta=location2[1]-location3[1];
        TranslateAnimation animation =new TranslateAnimation(fromXDelta,toXDelta,fromYDelta,toYDelta);
        animation.setDuration(400);
        toTv.setText(fromTv.getText());
        helperTv.setText(fromTv.getText());
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                fromTv.setVisibility(View.INVISIBLE);
                toTv.setVisibility(View.INVISIBLE);
                helperTv.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                fromTv.setVisibility(View.VISIBLE);
                toTv.setVisibility(View.VISIBLE);
                helperTv.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        helperTv.startAnimation(animation);

    }



    private void prepareList(final ArrayList<Contact> list) {
        list.clear();
        Comparator<Contact>contactComparator;
        if(sortAtoZ)contactComparator= ScreeninteractionAppcontroller.aToZ;
        else contactComparator= ScreeninteractionAppcontroller.ztoA;
        Collections.sort(ScreeninteractionAppcontroller.contactsList,contactComparator);
        int indexofNextFavorite=0;
        Character currentChar=null;
        for (int i = 0; i < ScreeninteractionAppcontroller.contactsList.size(); i++) {
            Contact currentContact= ScreeninteractionAppcontroller.contactsList.get(i);
            currentContact.favroite= ScreeninteractionAppcontroller.favoritePrefs.getBoolean(currentContact.name,false);
            if(currentContact.favroite){
                list.add(indexofNextFavorite++,currentContact);
            }
            else {
                if(currentChar==null||currentChar!=currentContact.name.toUpperCase().charAt(0)){
                    //add header
                    currentChar=currentContact.name.toUpperCase().charAt(0);
                    Contact sectionHeader=new Contact();
                    sectionHeader.isSectionHeader=true;
                    sectionHeader.secionChar=currentChar;
                    list.add(sectionHeader);
                }
                list.add(currentContact);
            }
        }


    }

    @Override
    public void onBackPressed() {

        if(detailsFragmentContainer.getVisibility()==View.VISIBLE){
            AlphaAnimation animation=new AlphaAnimation(1f,0f);
            animation.setDuration(300);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                detailsFragmentContainer.setVisibility(View.INVISIBLE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Window window = getWindow();
                        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                        window.setStatusBarColor(getResources().getColor(R.color.colorSplashBackGround));
                    };
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            detailsFragmentContainer.startAnimation(animation);
        }
        else{
            super.onBackPressed();
        }
    }
}
