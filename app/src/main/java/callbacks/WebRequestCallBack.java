package callbacks;

/**
 * Created by Gaber on 10/8/15.
 */
import android.widget.Toast;

import mhgaber.com.screeninteraction.R;
import models.ScreeninteractionAppcontroller;


/**
 * Created by Gaber on 4/30/15.
 */
public class WebRequestCallBack {
    public void onSuccess(String response) {
    }

    public void onError(String error) {
        Toast.makeText(ScreeninteractionAppcontroller.screeninteractionAppcontroller.getApplicationContext(), ScreeninteractionAppcontroller.screeninteractionAppcontroller.getString(R.string.InternetConnectionProblem), Toast.LENGTH_SHORT).show();
    }

    public void onError(Exception exception) {
        Toast.makeText(ScreeninteractionAppcontroller.screeninteractionAppcontroller.getApplicationContext(), ScreeninteractionAppcontroller.screeninteractionAppcontroller.getString(R.string.InternetConnectionProblem), Toast.LENGTH_SHORT).show();

    }

    public void onCancel() {
    }

}
