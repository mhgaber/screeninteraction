package mhgaber.com.screeninteraction;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

public class WebBrowserActivity extends AppCompatActivity {

WebView webView;
    TextView urlTv;
    ImageView cancelIv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_browser);
        String url=getIntent().getStringExtra("url");
        cancelIv=(ImageView)findViewById(R.id.cancel);
        cancelIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        urlTv=(TextView)findViewById(R.id.texViewUrl);
        urlTv.setText(url);
        webView= (WebView) findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(url);
    }

}
