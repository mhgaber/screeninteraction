package adapters;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import managers.ImageLoader;
import mhgaber.com.screeninteraction.R;
import models.ScreeninteractionAppcontroller;
import models.Contact;

/**
 * Created by Gaber on 10/8/15.
 */
public class ContactListAdapter  extends ArrayAdapter<Contact>{
ArrayList<Contact>list;
    public ContactListAdapter(Context context, int resource,ArrayList list) {
        super(context, resource);
        this.list=list;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Contact contact=getItem(position);
        if(getItemViewType(position)==1) return getSectionHeaderView(position);
        ViewHolder vh;
        if(convertView==null) {
            if (getItemViewType(position) == 0) {
                //contact
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.contact_row, null);
                vh = new ViewHolder();
                vh.favoriteIV = (ImageView) convertView.findViewById(R.id.imageViewFav);
                vh.profilePicIv = (ImageView) convertView.findViewById(R.id.imageViewProfileThump);
                vh.seperatorIv = (ImageView) convertView.findViewById(R.id.imageViewSeperatorLine);
                vh.jobTitle = (TextView) convertView.findViewById(R.id.textViewJob);
                vh.name = (TextView) convertView.findViewById(R.id.textViewName);
                convertView.setTag(vh);
            } else {
                return getSectionHeaderView(position);
            }
        }
            if(getItemViewType(position)==0){
                //contact

                vh=(ViewHolder)convertView.getTag();
                vh.jobTitle.setText(getItem(position).jobTitle);
                vh.name.setText(getItem(position).name);
                vh.profilePicIv.setTag(position);
                vh.profilePicIv.setImageBitmap(null);
                if(ScreeninteractionAppcontroller.favoritePrefs.getBoolean(contact.name,false)){
                    vh.favoriteIV.setImageResource(R.drawable.star1);
                }
                else vh.favoriteIV.setImageResource(R.drawable.star3);
                final ViewHolder finalVh = vh;
                vh.favoriteIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(ScreeninteractionAppcontroller.favoritePrefs.getBoolean(contact.name,false)){
                            ScreeninteractionAppcontroller.toggleFavorite(getItem(position).name, false);
                            finalVh.favoriteIV.setImageResource(R.drawable.star3);
                        }else{
                            ScreeninteractionAppcontroller.toggleFavorite(getItem(position).name, true);
                            finalVh.favoriteIV.setImageResource(R.drawable.star1);
                        }
                    }
                });
                ImageLoader.loadImage(vh.profilePicIv, getItem(position).thumbnailUrl, position, true, true);
                if(position==getCount()-1||getItemViewType(position+1)==1){
                    vh.seperatorIv.setVisibility(View.INVISIBLE);
                }
                else vh.seperatorIv.setVisibility(View.VISIBLE);
            }
            else{
                return getSectionHeaderView(position);
            }

        return convertView;
    }

    private View getSectionHeaderView(int position) {
        TextView textView=new TextView(getContext());
        textView.setBackgroundColor(getContext().getResources().getColor(R.color.colorLisSecionBackGround));
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setPadding(30, 30, 0, 30);
        textView.setText(getItem(position).secionChar + "");
        textView.setTextColor(getContext().getResources().getColor(R.color.colorMainText));
        return textView;
    }

    @Override
    public int getItemViewType(int position) {
        if(getItem(position).isSectionHeader)return 1;
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return  2;
    }

    @Override
    public Contact getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    private static class ViewHolder{
        ImageView profilePicIv,favoriteIV,seperatorIv;
        TextView name,jobTitle;
    }
}
